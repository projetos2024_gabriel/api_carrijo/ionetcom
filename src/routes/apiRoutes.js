const router = require('express').Router()
const teacherController = require('../controller/teacherController');
const alunoController = require('../controller/alunoController');
const JSONPlaceholderController = require('../controller/JSONPlaceholderController');

router.get('/teacher/', teacherController.getTeacher);
router.put('/cadastroAluno/', teacherController.updateAluno);
router.delete('/cadastroAluno/:id', alunoController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getUsersWebSiteIO);
router.get("/external/com", JSONPlaceholderController.getUsersWebSiteCOM);
router.get("/external/net", JSONPlaceholderController.getUsersWebSiteNET);
router.get('/external/filter', JSONPlaceholderController.getCountDomain);




module.exports = router